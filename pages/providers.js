import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Content from '../components/global/content';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

function createData(name, calories, fat, carbs, protein, comprador, proveedor, ramo, status) {
    return { name, calories, fat, carbs, protein, comprador, proveedor, ramo, status };
}

const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0, 'Antonio', 'Miguel', 'Noel', 'En proceso'),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3, 'Antonio', 'Miguel', 'Noel', 'En proceso'),
    createData('Eclair', 262, 16.0, 24, 6.0, 'Antonio', 'Miguel', 'Noel', 'En proceso'),
    createData('Cupcake', 305, 3.7, 67, 4.3, 'Antonio', 'Miguel', 'Noel', 'En proceso'),
    createData('Gingerbread', 356, 16.0, 49, 3.9, 'Antonio', 'Miguel', 'Noel', 'En proceso'),
];

export default function Providers() {
    const classes = useStyles();

    return (
        <Content>

            <Grid container spacing={1}>
                <Grid item xs={12}>
                    <h2>Proveedores</h2>
                </Grid>
                <Grid item xs={12}>
                    <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="right">Folio</TableCell>
                                    <TableCell align="right">Fecha de inicio</TableCell>
                                    <TableCell align="right">Fecha fin</TableCell>
                                    <TableCell align="right">Solicitante</TableCell>
                                    <TableCell align="right">Comprador</TableCell>
                                    <TableCell align="right">Proveedor</TableCell>
                                    <TableCell align="right">Ramo</TableCell>
                                    <TableCell align="right">Status</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.map((row) => (
                                    <TableRow key={row.name}>
                                        <TableCell align="right">{row.calories}</TableCell>
                                        <TableCell align="right">{row.fat}</TableCell>
                                        <TableCell align="right">{row.carbs}</TableCell>
                                        <TableCell align="right">{row.protein}</TableCell>
                                        <TableCell align="right">{row.comprador}</TableCell>
                                        <TableCell align="right">{row.proveedor}</TableCell>
                                        <TableCell align="right">{row.ramo}</TableCell>
                                        <TableCell align="right">{row.status}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>

        </Content>
    );
}
