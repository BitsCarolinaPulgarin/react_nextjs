import React from 'react';
import Content from '../components/global/content';
import Grid from '@material-ui/core/Grid';


export default function Home() {

  return (

    <Content>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <h2>Alta de proveedores</h2>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={8}>
          <img src="/images/grafica.jpg" />
        </Grid>
        <Grid item xs={4}>
          <img src="/images/calendario.jpg" />
        </Grid>
      </Grid>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <h2>Proveedores en proceso de alta</h2>
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        <Grid item xs>
          <img src="/images/grid1.jpg" width="260" height="190"/>
        </Grid>
        <Grid item xs>
          <img src="/images/grid2.jpg" width="260" height="190" />
        </Grid>
        <Grid item xs>
          <img src="/images/grid3.jpg" width="260" height="190"/>
        </Grid>
        <Grid item xs>
          <img src="/images/grid1.jpg" width="260" height="190"/>
        </Grid>
      </Grid>
    </Content>

  )
}
