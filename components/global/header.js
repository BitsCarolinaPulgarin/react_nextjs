import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Modal from '../forms/modal'
import Navigation from './navigation'



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        background: '#b71c1c',
        color: '#fff',
    },
    title: {
        flexGrow: 1,
    },
}));

export default function Header() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const ActionModal = (action) =>{
        setOpen(action)
    }

    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Toolbar>
                    <img src='/images/ab-inveb-logo.png' width="200px" />
                    <Typography variant="h6" className={classes.title}>
                        HAND TO HAND
                    </Typography>
                    <Button  className={classes.menuButton} variant="contained" onClick={() => setOpen(true)}>Nuevo proveedor</Button>
                </Toolbar>
            </AppBar>
            <Navigation />
            <Modal openModal={open}  action={ActionModal}/>
        </div>
    );
}
