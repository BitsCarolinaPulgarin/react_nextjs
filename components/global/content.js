import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Header from './header';


export default function Content(props) {
    return (
        <React.Fragment>
            <Header />
            <CssBaseline />
            <Container maxWidth="lg">
                {props.children}
            </Container>
        </React.Fragment>
    );
}
