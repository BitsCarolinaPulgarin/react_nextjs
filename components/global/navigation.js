import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import StorefrontIcon from '@material-ui/icons/Storefront';
import HomeIcon from '@material-ui/icons/Home';
import Link from 'next/link'

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
});



export default function Navigation() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    return (
        <BottomNavigation
            value={value}
            onChange={(event, newValue) => {
                setValue(newValue);
            }}
            showLabels
            className={classes.root}
        >
            <Link href='/'><BottomNavigationAction label="Inicio" icon={<HomeIcon />} /></Link>
            <Link href='/providers'><BottomNavigationAction label="Proveedores" icon={<StorefrontIcon />} /></Link>
        </BottomNavigation>
    );
}
