import React, { Component } from 'react'
import axios from 'axios';
import Fields from './fields_form';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

export default class Form extends Component {

    constructor() {
        super()

        this.state = {
            field: [],
            dataForm: {},
            alert: ''
        }
    }

    componentDidMount() {
        this.getField()
    }

    getField = () => {
        const URL = 'http://localhost/drupal/webform_rest/form_providers/fields';

        axios({
            method: 'GET',
            url: URL,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(r => {
            this.setState({ field: r.data })
        })

    }

    getForm = (data) => {

        let field = data.target.name
        let value = data.target.value

        for (const key in this.state.field) {

            if (key === field) {
                this.state.dataForm[key] = value

            }
        }

    }



    saveProvider = () => {

        const URL = 'http://localhost/drupal/webform_rest/submit';
        this.state.dataForm.webform_id = 'form_providers';

        axios({
            method: 'POST',
            url: URL,
            headers: {
                'Content-Type': 'application/json',
            },
            data: JSON.stringify(this.state.dataForm)
        }).then(r => {

            if (r.status === 200) {
                this.setState({ alert: 'registro guardado' })
            }

        }).catch(() => this.setState({ alert: 'No se pudo almacenar' }))
    }


    render() {
        return (
            <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="stretch"
                spacing={1}
            >
                {
                    Object.keys(this.state.field).map((r, key) => {
                        return <Fields key={key} className="form" data={this.state.field[r]} getform={this.getForm} />

                    })
                }

                <div>
                    <p>{this.state.alert}</p>
                </div>

            </Grid>
        )
    }
}
