import React, { Fragment, useState } from 'react'
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormLabel from '@material-ui/core/FormLabel';
import Select from '@material-ui/core/Select';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    }
}));


function Fields({ data, getform }) {

    const classes = useStyles();

    console.log(data['#type'])

    switch (data['#type']) {

        case 'textfield':

            return (
                <Fragment>
                    <Grid item xs={12}>
                        <FormLabel> {data["#title"]}  </FormLabel>
                    </Grid>

                    <Grid item xs={12}>
                        <TextField
                            id="outlined-margin-normal"
                            placeholder={data["#title"]}
                            className={classes.textField}
                            margin="normal"
                            variant="outlined"
                            name={data["#webform_key"]} onChange={getform}
                        />
                    </Grid>
                </Fragment>)

        case 'email':

            return (<Fragment>
                <Grid item xs={12}>
                    <FormLabel>{data["#title"]}</FormLabel>
                </Grid>

                <Grid item xs={12}>
                    <TextField
                        type="email"
                        id="outlined-margin-normal"
                        placeholder={data["#title"]}
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name={data["#webform_key"]} onChange={getform}
                    />
                </Grid>
            </Fragment>);

        case 'radios':

            return <Fragment>

                <Grid item xs={12}>
                    <FormLabel>{data["#title"]}</FormLabel>
                </Grid>


                {
                    Object.keys(data["#options"]).map((r, index) => {

                        return <Grid>
                            <FormLabel key={index}> {data["#options"][r]}
                                <input type="radio" name={data["#webform_key"]} value={r} onClick={getform} />
                            </FormLabel></Grid>
                    })
                }

            </Fragment>

        case "select":

            return <Fragment>

                <Grid item xs={12}>
                    <FormLabel>{data["#title"]}</FormLabel>
                </Grid>

                <Select labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined" onClick={getform}>

                    {
                        Object.keys(data["#options"]).map((r, index) => {

                            return <option value={data["#options"][r]}>{data["#options"][r]}</option>
                        })
                    }

                </Select>

            </Fragment >

        default:
            break;
    }

    return ""
}


export default Fields